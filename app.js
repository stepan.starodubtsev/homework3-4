let firstNumber = prompt("Введіть перше число:");
let secondNumber = prompt("Введіть друге число:");

do {
    if (isNaN(firstNumber)) {
        firstNumber = prompt("Введіть перше число:");
        firstNumber = parseInt(firstNumber);
    } else if (isNaN(secondNumber)) {
        secondNumber = prompt("Введіть друге число:");
        secondNumber = parseInt(secondNumber);
    } else {
        if (firstNumber < secondNumber) {
            console.log(secondNumber + "\n" + firstNumber)
        } else {
            console.log(firstNumber + "\n" + secondNumber)
        }
        break;
    }
} while (true);


let inputNumber;
do {
    inputNumber = prompt("Введіть парне число:");
    if (!isNaN(inputNumber)) {
        if (inputNumber % 2 === 0) {
            break;
        }
    }
} while (true);

console.log(inputNumber);